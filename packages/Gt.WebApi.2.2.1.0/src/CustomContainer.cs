﻿using System;
using System.Web.Http.Dependencies;
using Gt.IoC;
using Microsoft.Practices.Unity;
using Unity.WebApi;

namespace Gt.WebApi
{
    internal class CustomContainer: IocContainer
    {
        private readonly UnityDependencyResolver _resolver;

        public CustomContainer(IUnityContainer container) : base(container)
        {
            _resolver = new UnityDependencyResolver(container);
        }

        public override object GetService(Type serviceType)
        {
            return _resolver.GetService(serviceType);
        }

        public IDependencyResolver DependencyResolver => _resolver;
    }
    
}
