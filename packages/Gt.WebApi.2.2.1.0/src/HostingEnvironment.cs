﻿using System;
using System.IO;
using System.Web;
using System.Web.Http.Controllers;
using Gt.IoC;
using Gt.WebApi.Attributes;
using Gt.WebApi.Interfaces;

namespace Gt.WebApi
{
    [Singleton]
    public class HostingEnvironment: IHostingEnvironment
    {
        public string MapServerPath(string path)
        {
            return Path.Combine(HttpRuntime.AppDomainAppPath, path);
        }

        public Uri GetServiceUri()
        {
            var context = HttpContext.Current;
            if (context == null) return null;

            var url = context.Request.Url;
            return new Uri($"{url.Scheme}://{url.Host}:{url.Port}/", UriKind.Absolute);
        }
    }
}