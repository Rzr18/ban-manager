using System.Web.Configuration;
using Gt.IoC;

namespace Gt.WebApi
{
    internal class WebApiConfiguration : Configuration.Configuration, IWebApiConfiration
    { 
        public WebApiConfiguration(string fileName): base(new HostingEnvironment().MapServerPath(fileName))
        {
        }

        public string GetConnectionString(string name)
        {
            var settings = WebConfigurationManager.ConnectionStrings[name];
            return settings.ConnectionString;
        }
    }
}