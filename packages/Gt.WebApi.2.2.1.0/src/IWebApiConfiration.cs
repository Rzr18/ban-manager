using Gt.Configuration;

namespace Gt.WebApi
{
    public interface IWebApiConfiration : IConfiguration
    {
        string GetConnectionString(string name);
    }
}