﻿using System;

namespace Gt.WebApi.Interfaces
{
    public interface IHostingEnvironment
    {
        string MapServerPath(string path);
        Uri GetServiceUri();
    }
}