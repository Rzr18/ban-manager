﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Gt.WebApi.Attributes
{
    public class ValidateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            var requiredParams =
                context.ActionDescriptor.GetParameters()
                    .Where(p => p.GetCustomAttributes<RequiredAttribute>().Any())
                    .Select(p => p.ParameterName)
                    .ToList();

            var nullRequireds = context.ActionArguments.Where(a => a.Value == null && requiredParams.Contains(a.Key));

            foreach (var nullRequired in nullRequireds)
            {
                context.ModelState.AddModelError("param_" + nullRequired.Key, "Не задан обязательный параметр");
            }

            if (context.ModelState.IsValid) return;

            var errors = context.ModelState.Where(m => m.Value.Errors.Any())
                .ToDictionary(x => x.Key, x => x.Value.Errors.Select(e => new {Message = e.ErrorMessage, e.Exception}));
            context.Response = context.Request.CreateResponse(HttpStatusCode.BadRequest, errors,
                context.ControllerContext.Configuration.Formatters.JsonFormatter);
        }
    }
}