using System.Web.Http;
using Gt.WebApi.ActionResults;

namespace Gt.WebApi.Extensions
{
    public static class ApiControllerExtensions
    {
        public static IHttpActionResult File(this ApiController controller, string filename, string contentType)
        {
            return new FileHttpActionResult(controller.Request, filename, contentType);
        }
    }
}