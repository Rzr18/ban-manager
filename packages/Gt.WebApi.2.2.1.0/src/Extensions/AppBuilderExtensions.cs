using System;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Gt.Configuration;
using Gt.IoC;
using Gt.WebApi.Attributes;
using Microsoft.Practices.Unity;


namespace Gt.WebApi.Extensions
{
    public static class AppBuilderExtensions
    {
        public static IServiceCollection ConfigureApi(this HttpConfiguration config, Type assemblyTag=null, Action<IServiceCollection> configAction=null)
        {
            config.MapHttpAttributeRoutes();

            var container = new UnityContainer();
            var customContainer = new CustomContainer(container);
            var resolver = customContainer.DependencyResolver;
            config.DependencyResolver = resolver;

            var configJson = new WebApiConfiguration("config.json");
            customContainer.AddSingleton(typeof(IConfiguration), configJson);
            customContainer.AddSingleton(typeof(IWebApiConfiration), configJson);
            
            configAction?.Invoke(customContainer);
            IoC.Extensions.ServicesExtensions.AddRegisteredWithAttributes(customContainer, typeof(AppBuilderExtensions));
            IoC.Extensions.ServicesExtensions.AddRegisteredWithAttributes(customContainer, assemblyTag?? Assembly.GetEntryAssembly().DefinedTypes.First());
            config.Filters.Add(new ValidateAttribute());

            return customContainer;
        }

        public static IStartup Startup(this HttpConfiguration config)
        {
            var resolver = config.DependencyResolver;
            if (resolver == null) return null;

            var startup = (IStartup)resolver.GetService(typeof (IStartup));
            startup?.Startup();
            return startup;
        }

        public static T GetService<T>(this HttpConfiguration config) where T: class
        {
            return (T)config.DependencyResolver?.GetService(typeof(T));
        }
    }

    
}