﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Gt.Base.Extensions
{
    public static class CollectionsExtensions
    {
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            foreach (var element in elements)
                collection.Add(element);
        }

        public static void AddDisctinct<T>(this ICollection<T> collection, T element)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (element == null) throw new ArgumentNullException(nameof(element));
            if (!collection.Contains(element))
                collection.Add(element);
        }

        public static void AddRange(this IList collection, IEnumerable elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            foreach (var element in elements)
                collection.Add(element);
        }

        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));
            if (action == null) throw new ArgumentNullException(nameof(action));
            foreach (var item in items)
                action(item);
        }

        public static void AddRangeDistinct<T>(this ICollection<T> collection, IEnumerable<T> elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            collection.AddRange(elements.Except(collection));
        }

        public static void AddRangeDistinct(this IList collection, IEnumerable elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            collection.AddRange(elements.OfType<object>().Except(collection.OfType<object>()));
        }

        public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T> elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));

            foreach (var element in elements)
                collection.Remove(element);
        }

        public static void RemoveRange(this IList collection, IEnumerable elements)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (elements == null) throw new ArgumentNullException(nameof(elements));
            foreach (var element in elements)
                collection.Remove(element);
        }

        public static int RemoveAll<T>(this ICollection<T> collection, Func<T, bool> condition)
        {
            if (collection == null) throw new ArgumentNullException(nameof(collection));
            if (condition == null) throw new ArgumentNullException(nameof(condition));
            var array = collection.Where(condition).ToArray();
            foreach (var item in array)
                collection.Remove(item);
            return array.Length;
        }

        private static readonly Random Rnd = new Random(Environment.TickCount);

        /// <summary>
        /// Выполняет рандомизацию списка
        /// </summary>
        /// <param name="list">Список который надо рандомизировать</param>
        /// <typeparam name="T">Тип элемента списка</typeparam>
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            if (list == null) return null;
            var count = list.Count;
            while (count > 1)
            {
                count--;
                var k = Rnd.Next(count + 1);
                var value = list[k];
                list[k] = list[count];
                list[count] = value;
            }
			return list;
        }

        public static bool NotNullOrEmpty<T>(this T[] array) =>
            array != null && array.Length != 0;

        public static bool NotNullOrEmpty<T>(this ICollection<T> collection) =>
            collection != null && collection.Count != 0;

        public static bool IsNullOrEmpty<T>(this T[] array) =>
            array == null || array.Length == 0;

        public static bool IsNullOrEmpty<T>(this ICollection<T> collection) =>
            collection == null || collection.Count == 0;

        /// <summary>
        /// Выполняет рандомизацию списка
        /// </summary>
        /// <param name="items">Список который надо рандомизировать</param>
        /// <typeparam name="T">Тип элемента списка</typeparam>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> items)
        {
            if (items == null) return null;
            var list = items.ToList();
            var count = list.Count;
            while (count > 1)
            {
                count--;
                var k = Rnd.Next(count + 1);
                var value = list[k];
                list[k] = list[count];
                list[count] = value;
            }
			return list;
        }

    }
}