﻿using System;
using System.Collections.Specialized;

namespace Gt.Base.Extensions
{
    public static class ObjectExtensions
    {
        public static NameValueCollection ToNameValueCollection(this object obj)
        {
            var properties = obj.GetType().GetProperties();
            var collection = new NameValueCollection();
            foreach (var property in properties)
                collection.Add(property.Name, Convert.ToString(property.GetValue(obj)));
            return collection;
        }
    }
}