using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Gt.Base.Extensions
{
    public static class HtmlExtensions
    {
        /// <summary>
        /// �������� ���� �� ������
        /// </summary>
        /// <param name="text">�����</param>
        /// <returns>����� ��� �����</returns>
        public static string StripTags(this string text)
        {
            var regHtml = new Regex("<[^>]*>");
            return regHtml.Replace(text, string.Empty);
        }

        public static string Md5(this string text)
        {
            var textBytes = Encoding.Default.GetBytes(text);
            return textBytes.Md5();
        }

        public static string Md5(this byte[] buffer)
        {
            using (var cryptHandler = new MD5CryptoServiceProvider())
            {
                var hash = cryptHandler.ComputeHash(buffer);
                return hash.ToHexString();
            }
        }

        public static string ToHexString(this byte[] array)
        {
            var sb = new StringBuilder();
            foreach (var a in array)
            {
                if (a < 16)
                    sb.Append("0" + a.ToString("x"));
                else
                    sb.Append(a.ToString("x"));
            }
            return sb.ToString();
        }

        public static string Md5(this Stream stream)
        {
            using (var cryptHandler = new MD5CryptoServiceProvider())
            {
                var hash = cryptHandler.ComputeHash(stream);
                return hash.ToHexString();
            }
        }

    }
}