﻿using System;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Gt.Base.Extensions
{
    [TypeForwardedFrom("Gt.Base.HttpClientExtensions")]
    public static class HttpClientExtensions
    {
        public static async Task<T> GetAsync<T>(this HttpClient client,string uri) where T:class
        {
            var response = await client.GetAsync(uri);
            if (!response.IsSuccessStatusCode || response.Content == null)
            {
                throw await Error("GET", uri, response);
            }

            var content = await response.Content.ReadAsStringAsync();
            return await content.FromJson<T>();
        }

        public static async Task PostAsync(this HttpClient client, string uri,object data = null)
        {
            var response = await client.PostAsync(uri, data!=null? await data.ToContent():null);
            if (!response.IsSuccessStatusCode || response.Content == null)
            {
                throw await Error("POST", uri, response);
            }
        }

        public static async Task<TResult> PostAsync<TResult>(this HttpClient client, string uri, object data = null)
        {
            var response = await client.PostAsync(uri, data != null ? await data.ToContent() : null);
            if (!response.IsSuccessStatusCode || response.Content == null)
            {
                throw await Error("POST", uri, response);
            }
            var content = await response.Content.ReadAsStringAsync();
            return await content.FromJson<TResult>();
        }


        public static async Task PutAsync(this HttpClient client, string uri, object data = null)
        {
            var response = await client.PutAsync(uri, data != null ? await data.ToContent() : null);
            if (!response.IsSuccessStatusCode || response.Content == null)
            {
                throw await Error("PUT", uri, response);
            }
        }


        public static async Task<T> PutAsync<T>(this HttpClient client, string uri, object data = null)
        {
            var response = await client.PutAsync(uri, data != null ? await data.ToContent() : null);
            if (!response.IsSuccessStatusCode || response.Content == null)
            {
                throw await Error("PUT", uri, response);
            }
            var content = await response.Content.ReadAsStringAsync();
            return await content.FromJson<T>();
        }

        private static async Task<Exception> Error(string method, string uri, HttpResponseMessage message)
        {
            var ex = new HttpRequestException("Ошибка при выполнении запроса");
            ex.Data["Status"] = message.StatusCode;
            ex.Data["Content"] = message.Content != null ? await message.Content.ReadAsStringAsync() : string.Empty;
            ex.Data["Uri"] = uri;
            ex.Data["Method"] = method;
            return ex;
        }
    }

    
}
