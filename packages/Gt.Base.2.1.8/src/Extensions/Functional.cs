﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gt.Base.Maybees;

namespace Gt.Base.Extensions
{
    public static class Functional
    {
        /// <summary>
        /// Произвести проверку условий
        /// </summary>
        /// <typeparam name="T">тип значения</typeparam>
        /// <typeparam name="TR">тип результата</typeparam>
        /// <param name="obj">объект</param>
        public static Match<T, TR> Match<T, TR>(this T obj)
        {
            return new Match<T, TR>(obj);
        }

        /// <summary>
        /// Произвести проверку условий
        /// </summary>
        /// <typeparam name="T">тип значения</typeparam>
        /// <param name="obj">объект</param>
        public static Match<T> Match<T>(this T obj)
        {
            return new Match<T>(obj);
        }

        /// <summary>
        /// Метод - заглушка
        /// </summary>
        public static void Ignore<T>(this T value)
        {
            //Метод - заглушка
        }

        /// <summary>
        /// Метод - заглушка
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        public static void Use<T>(this T value)
        {
            //Метод - заглушка
        }

        /// <summary>
        /// Вызвать для данного значения указанное действие и вернуть значение
        /// </summary>
        /// <typeparam name="T">тип значения</typeparam>
        /// <param name="value">значение</param>
        /// <param name="action">действие</param>
        /// <returns>значение</returns>
        public static T Invoke<T>(this T value, Action<T> action)
        {
            action(value);
            return value;
        }

        /// <summary>
        /// Первое значение последовательности или пустое значение
        /// </summary>
        public static Maybe<TSource> FirstOrEmpty<TSource>(this IEnumerable<TSource> source)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            var list = source.ToList();
            return list.Match<IEnumerable<TSource>, Maybe<TSource>>()
                .Case<IList<TSource>>(l => l.Count > 0
                                               ? l[0]
                                               : Maybe<TSource>.Empty)
                .Default(e =>
                {
                    using (var enumerator = list.GetEnumerator())
                    {
                        if (enumerator.MoveNext())
                            return enumerator.Current;
                    }
                    return Maybe<TSource>.Empty;
                });
        }
    }
}
