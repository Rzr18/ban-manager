using System;
using System.ComponentModel;
using static System.String;

namespace Gt.Base.Extensions
{
    /// <summary>
    /// ����� ������� ����������
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// ��������� ����� �������� Description � �������� ������������
        /// </summary>
        /// <param name="value">�������� ������������</param>
        /// <returns>����� �������� Description, ����� ��� ��������</returns>
        public static string GetEnumDescription(this Enum value)
        {
            return GetObjectDescription(value);
        }

        /// <summary>
        /// ��������� ����� �������� Description � �������� ������������
        /// </summary>
        /// <param name="value">�������� ������������</param>
        /// <returns>����� �������� Description, ����� ��� ��������</returns>
        public static string GetObjectDescription(this object value)
        {
            var entries = value.ToString().Split(',');
            var description = new string[entries.Length];
            for (var i = 0; i < entries.Length; i++)
            {
                var fieldInfo = value.GetType().GetField(entries[i].Trim());
                var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

                description[i] = (attributes.Length > 0) ? attributes[0].Description : entries[i].Trim();
            }
            return Join(", ", description);
        }
    }
}