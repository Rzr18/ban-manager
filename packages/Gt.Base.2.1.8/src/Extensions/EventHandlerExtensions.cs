﻿using System;

namespace Gt.Base.Extensions
{
    /// <summary>
    /// Класс методов расширения для вызова событий
    /// </summary>
    public static class EventHandlerExtensions
    {
        /// <summary>
        /// Вызов события без параметров
        /// </summary>
        /// <param name="handler">событие</param>
        /// <param name="sender">источник</param>
        public static void Raise(this EventHandler handler, object sender)
        {
            handler?.Invoke(sender, EventArgs.Empty);
        }


        /// <summary>
        /// Вызов события с параметром
        /// </summary>
        /// <typeparam name="T">тип параметра</typeparam>
        /// <param name="handler">событие</param>
        /// <param name="sender">источник</param>
        /// <param name="args">параметры</param>
        public static void Raise<T>(this EventHandler<T> handler, object sender, T args) where T : EventArgs
        {
            handler?.Invoke(sender, args);
        }
    }
}