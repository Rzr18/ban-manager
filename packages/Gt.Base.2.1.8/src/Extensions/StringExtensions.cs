﻿using System;

namespace Gt.Base.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64String(this byte[] bytes)
        {
            return bytes == null || bytes.Length == 0 ? string.Empty : Convert.ToBase64String(bytes);
        }

        public static byte[] FromBase64String(this string text)
        {
            return string.IsNullOrEmpty(text) ? new byte[0] : Convert.FromBase64String(text);
        }

    }
}