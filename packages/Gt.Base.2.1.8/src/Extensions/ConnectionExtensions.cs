﻿using System.Data;

namespace Gt.Base.Extensions
{
    public static class ConnectionExtensions
    {
        public static int ExecuteNonQuery(this IDbConnection connection, string command)
        {
            var cmd = connection.CreateCommand();
            cmd.CommandText = command;
            return cmd.ExecuteNonQuery();
        }
    }
}