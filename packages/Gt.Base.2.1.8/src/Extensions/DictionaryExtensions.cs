﻿using System;
using System.Collections.Generic;
using Gt.Base.Maybees;

namespace Gt.Base.Extensions
{
    public static class DictionaryExtensions
    {
        public static Maybe<T> TryGetValue<TKey, T>(this IDictionary<TKey, T> dictionary, TKey key)
        {
            T value;
            return dictionary.TryGetValue(key, out value) ? value : Maybe<T>.Empty;
        }

        public static T GetValueOrNull<TKey, T>(this IDictionary<TKey, T> dictionary, TKey key) where T: class
        {
            T value;
            return dictionary.TryGetValue(key, out value) ? value : null;
        }

        public static T GetValueOrDefault<TKey, T>(this IDictionary<TKey, T> dictionary, TKey key, T defaultValue = default(T))
        {
            T value;
            return dictionary.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static TExpr GetExpressionOrDefault<TKey, T, TExpr>(this IDictionary<TKey, T> dictionary, TKey key, Func<T, TExpr> expression)
        {
            return GetExpressionOrDefault(dictionary, key, expression, default(TExpr));
        }

        public static TExpr GetExpressionOrDefault<TKey, T, TExpr>(this IDictionary<TKey, T> dictionary, TKey key, Func<T, TExpr> expression, TExpr defaultValue)
        {
            T value;
            return dictionary.TryGetValue(key, out value) ? expression(value) : defaultValue;
        }

        public static TValue GetOrCreateValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key,
            Func<TValue> createMethod)
        {
            TValue value;
            if (dictionary.TryGetValue(key, out value)) return value;

            value = createMethod();
            dictionary.Add(key, value);
            return value;
        }
    }
}