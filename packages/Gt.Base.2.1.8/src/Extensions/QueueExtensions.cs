using System;
using System.Collections.Generic;

namespace Gt.Base.Extensions
{
    /// <summary>
    /// ����� ������� ���������� ���� <c>Queue</c>
    /// </summary>
    public static class QueueExtensions
    {
        /// <summary>
        /// ��������� ���������� ������������ ��������� � �������.
        /// </summary>
        /// <typeparam name="T">��� ���������</typeparam>
        /// <param name="queue">��������� ������� ���������</param>
        /// <param name="elements">������������ ���������, ������� ���������� ��������</param>
        public static void EnqueueRange<T>(this Queue<T> queue, IEnumerable<T> elements)
        {
            if (queue == null)
                throw new ArgumentNullException(nameof(queue));
            if (elements == null)
                throw new ArgumentNullException(nameof(elements));
            foreach (var element in elements)
                queue.Enqueue(element);
        }
    }
}