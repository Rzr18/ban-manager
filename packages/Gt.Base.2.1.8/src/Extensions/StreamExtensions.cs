﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Gt.Base.Extensions
{
    public static class StreamExtensions
    {
        private const int BufferSize = 32768;

        public static byte[] ReadAllBytes(this Stream stream)
        {
            using (stream)
            {
                var buffer = new byte[BufferSize];
                using (var output = new MemoryStream())
                {
                    while (true)
                    {
                        var read = stream.Read(buffer, 0, buffer.Length);
                        if (read <= 0)
                            return output.ToArray();
                        output.Write(buffer, 0, read);
                    }
                }
            }
        }

        public static async Task<byte[]> ReadAllBytesAsync(this Stream stream)
        {
            using (stream)
            {
                var buffer = new byte[BufferSize];
                using (var output = new MemoryStream())
                {
                    while (true)
                    {
                        var read = await stream.ReadAsync(buffer, 0, buffer.Length);
                        if (read <= 0)
                            return output.ToArray();
                        await output.WriteAsync(buffer, 0, read);
                    }
                }
            }
        }

        public static string ReadToEnd(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static Task<string> ReadToEndAsync(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEndAsync();
            }
        }

        /// <summary>
        /// Выполняет сохранение потока в указанная файл.
        /// Если файл уже существует он будет перезаписан.
        /// </summary>
        /// <param name="input">Поток данных</param>
        /// <param name="filename">Имя файла, в который неободимо сохранить поток данных</param>
        /// <exception cref="ArgumentNullException"/>
        /// <exception cref="ArgumentException"/>
        public static void SaveToFile(this Stream input, string filename)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (string.IsNullOrWhiteSpace(filename))
                throw new ArgumentException(@"Не задано имя файла", nameof(filename));
            using (var output = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                int len;
                var buffer = new byte[8 * 1024];
                while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
                    output.Write(buffer, 0, len);
            }
        }

        /// <summary>
        /// Выполняет сохранение потока в указанная файл.
        /// Если файл уже существует он будет перезаписан.
        /// </summary>
        /// <param name="input">Поток данных</param>
        /// <param name="filename">Имя файла, в который неободимо сохранить поток данных</param>
        /// <exception cref="ArgumentNullException"/>
        /// <exception cref="ArgumentException"/>
        public static async Task SaveToFileAsync(this Stream input, string filename)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));
            if (string.IsNullOrWhiteSpace(filename))
                throw new ArgumentException(@"Не задано имя файла", nameof(filename));
            using (var output = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                int len;
                var buffer = new byte[8 * 1024];
                while ((len = await input.ReadAsync(buffer, 0, buffer.Length)) > 0)
                    await output.WriteAsync(buffer, 0, len);
            }
        }
    }
}