﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Gt.Base.Extensions
{
    public static class JsonExtensions
    {
        public static Task<T> FromJson<T>(this string content)
        {
            return Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(content));
        }

        public static async Task<HttpContent> ToContent<T>(this T obj)
        {
            var json = await obj.ToJson();
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        public static Task<string> ToJson<T>(this T obj, Formatting formatting=Formatting.None)
        {
            return Task.Factory.StartNew(
                                            () => formatting==Formatting.None 
                                                ? JsonConvert.SerializeObject(obj) 
                                                : JsonConvert.SerializeObject(obj, formatting)
                                        );
        }
    }
}
