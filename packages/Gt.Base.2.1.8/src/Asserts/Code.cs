﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Gt.Base.Asserts
{
    public static class Code
    {
        [DebuggerHidden, MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void NotNull<T>(T obj, string argName) where T : class
        {
            if (argName == null)
                throw new ArgumentNullException(nameof(argName));
            if (obj == null)
                throw new ArgumentException($"All items in '{argName}' should not be null.", argName);
        }

        [DebuggerHidden, MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void NotNullOrEmpty(string obj, string argName)
        {
            if (argName == null)
                throw new ArgumentNullException(nameof(argName));
            if (string.IsNullOrEmpty(obj))
                throw new ArgumentException($"String '{argName}' should be neither null nor empty", argName);
        }

        [DebuggerHidden, MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void NotNullOrWhitespace(string obj, string argName)
        {
            if (argName == null)
                throw new ArgumentNullException(nameof(argName));
            if (string.IsNullOrWhiteSpace(obj))
                throw new ArgumentException($"String '{argName}' should be neither null nor whitespace", argName);
        }

        [DebuggerHidden, MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void InRange(
            int value,
            string argName,
            int fromValue,
            int toValue)
        {
            if (value < fromValue || value > toValue)
                throw new ArgumentOutOfRangeException(argName, value, $"The value of '{argName}' ({value}) should be between {fromValue} and {toValue}");
        }
    }
}
