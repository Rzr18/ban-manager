﻿using System;

namespace Gt.Base
{
    /// <summary>
    /// Параметы события с произвольным типом данных
    /// </summary>
    /// <typeparam name="T">тип данных - параметр события</typeparam>
    public class DataEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Данные
        /// </summary>
        public T Data { get; private set; }

        /// <summary>
        /// Создание экземпляра <c>DataEventArgs</c>
        /// </summary>
        /// <param name="data">данные</param>
        public DataEventArgs(T data)
        {
            Data = data;
        }
    }
}