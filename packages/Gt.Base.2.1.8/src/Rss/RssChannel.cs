﻿using System;
using System.Collections.Generic;

namespace Gt.Base.Rss
{
    public class RssChannel
    {
        public RssChannel(string title, string description, string link, DateTime date, IEnumerable<RssItem> items)
        {
            Title = title;
            Description = description;
            Link = link;
            Date = date;
            Items = items;
        }

        public string Description { get; }

        public IEnumerable<RssItem> Items { get; }

        public DateTime Date { get; }

        public string Link { get; }

        public string Title { get; }
    }
}