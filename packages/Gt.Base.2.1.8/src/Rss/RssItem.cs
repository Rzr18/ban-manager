using System;

namespace Gt.Base.Rss
{
    public struct RssItem
    {
        public RssItem(string title, string link, string description, DateTime date)
        {
            Title = title;
            Link = link;
            Description = description;
            Date = date;
        }

        public DateTime Date { get; }

        public string Description { get; }

        public string Link { get; }

        public string Title { get; }

        public override string ToString()
        {
            return Title;
        }


    }
}