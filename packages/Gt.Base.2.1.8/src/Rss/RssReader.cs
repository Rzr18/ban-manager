using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Gt.Base.Rss
{
    public static class RssReader
    {
        public static async Task<RssChannel> ReadChannel(Uri url)
        {
            return await ReadChannel(url.AbsoluteUri);
        }

        public async static Task<RssChannel> ReadChannel(string url)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var data = await client.GetStringAsync(url);
                    if (data == null) return null;

                    var doc = XDocument.Parse(data);

                    var root = doc.Elements().FirstOrDefault();
                    if (root == null)
                        return null;
                    var channelElement = GetElement(root, "channel");
                    var channel = LoadItem(channelElement);
                    var items = channelElement.Elements("item").Select(LoadItem);
                    return new RssChannel(channel.Title, channel.Description, channel.Link, channel.Date, items);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex);
                return null;
            }
        }

        private static RssItem LoadItem(XElement element)
        {
            DateTime date;
            DateTime.TryParse(TryGetElement(element, "pubDate").Value, out date);
            return new RssItem
                (
                GetElement(element, "title").Value.Replace("&#34;", "\""),
                GetElement(element, "link").Value,
                GetElement(element, "description").Value.Replace("&#34;", "\""),
                date
                );

        }

        private static XElement GetElement(XContainer container, string name)
        {
            var el = container.Element(name);
            if (el == null)
                throw new InvalidDataException($"Invalid rss channel format. Cannot find element '{name}'");
            return el;
        }

        private static XElement TryGetElement(XContainer container, string name)
        {
            var el = container.Element(name);
            return el ?? new XElement("temp") { Value = string.Empty };
        }

    }
}