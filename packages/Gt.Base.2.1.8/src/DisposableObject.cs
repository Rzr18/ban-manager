﻿using System;

namespace Gt.Base
{
    /// <summary>
    /// Базовый класс объекта механизма освобождения занимаемых ресурсво
    /// </summary>
    [Serializable]
    public abstract class DisposableObject : IDisposable
    {
        private bool _disposed;

        /// <summary>
        /// Выбрасывает исключение если объект уже был уничтожен
        /// </summary>
        /// <exception cref="ObjectDisposedException"/>
        protected void ThrowIfDisposed()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().Name);
        }

        /// <summary>
        /// Выполняет освобождение занимаемых ресурсов
        /// </summary>
        /// <param name="disposing">Требуется ли выполнять освобождение управляемых ресурсов</param>
        protected virtual void Dispose(bool disposing)
        {
            _disposed = true;
        }

        /// <summary>
        /// Выполняет освобождение занимаемых ресурсов
        /// </summary>
        public void Dispose()
        {
            if (!_disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
                _disposed = true;
            }
        }

        /// <summary>
        /// Деструктор типа
        /// </summary>
        ~DisposableObject()
        {
            if (!_disposed)
            {
                Dispose(false);
                _disposed = true;
            }
        }
    }
}