using System;
using System.Collections.Generic;

namespace Gt.Base
{
    public static class DictionaryExtensions
    {
        public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) where TValue : class
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : null;
        }

        public static TValue TryAddValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> factory) where TValue : class
        {
            var val = dictionary.TryGetValue(key);
            if (val == null)
            {
                val = factory();
                dictionary.Add(key, val);
            }
            return val;

        }
    }
}