﻿using System;
using System.IO;
using System.Security.Cryptography;
using Gt.Base.Extensions;

namespace Gt.Base.Helpers
{
    /// <summary>
    /// Вспомогательный класс подсчёта контрольноый суммы MD5
    /// </summary>
    public static class Md5Helper
    {
        /// <summary>
        /// Выполняет подсчёт контрольной суммы MD5 заданного файла
        /// </summary>
        /// <param name="filename">
        /// Имя файла, у которого требуется подсчитать
        /// контрольную сумму MD5
        /// </param>
        /// <returns>Строку MD5</returns>
        /// <exception cref="ArgumentException"/>
        /// <exception cref="InvalidOperationException"/>
        public static string CalcMd5(string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                throw new ArgumentException(@"Не задано имя файла для подсчёта контрольноый суммы", nameof(filename));
            if (!File.Exists(filename))
                throw new InvalidOperationException($"Файл '{filename}' не найден");
            using (var stream = File.OpenRead(filename))
            {
                return stream.Md5();    
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public static string CheckSum(byte[] buffer)
        {
            return buffer.Md5();
        }
    }
}