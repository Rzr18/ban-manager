﻿using System.IO;
using System.IO.Compression;

namespace Gt.Base.Helpers
{
    /// <summary>
    /// Класс помошник для упаковки/запаковки
    /// </summary>
    public static class ZipHelper
    {
        public static Stream ToZip(string file)
        {
            var memory = new MemoryStream();
            using (var gzip = new GZipStream(memory, CompressionMode.Compress, true))
            using (var fs = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                fs.CopyTo(gzip);

            memory.Position = 0;
            return memory;
        }

        public static void FromZip(Stream zippedStream, string filePath)
        {
            using (var ungzip = new GZipStream(zippedStream, CompressionMode.Decompress, true))
            using (var outFile = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                ungzip.CopyTo(outFile);
        }
    }
}
