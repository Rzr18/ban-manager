﻿using System;
using System.Text;

namespace Gt.Base
{
    public static class ExceptionFormatter
    {
        public static string Format(this Exception ex)
        {
            var sb = new StringBuilder();
            var e = ex;
            if (e == null) return string.Empty;

            while (e != null)
            {
                sb.AppendFormat($"{e.Message}\r\n{e.StackTrace}\r\n");
                e = e.InnerException;
            }
            if (ex?.Data == null) return sb.ToString();

            var data = ex.Data;
            sb.Append("Extended data:\r\n");
            foreach (var key in data.Keys)
            {
                var value = data[key];
                sb.AppendFormat("{0} : {1}\r\n", key, value);
            }
            return sb.ToString();
        }
    }
}
