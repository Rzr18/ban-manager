﻿using System.Collections.Generic;

namespace Gt.Base
{
    public interface IMultiDictionary<TKey, TValue> : ICollection<KeyValuePair<TKey, TValue>>
    {
        /// <summary>
        /// Получить колекцию значений по ключу
        /// </summary>
        /// <param name="key">ключ</param>
        IEnumerable<TValue> this[TKey key] { get; }

        /// <summary>
        /// Коллекция ключей
        /// </summary>
        IEnumerable<TKey> Keys { get; }

        /// <summary>
        /// Коллекция значений
        /// </summary>
        IEnumerable<TValue> Values { get; }

        /// <summary>
        /// Удалить все значения по ключу
        /// </summary>
        /// <param name="key">ключ</param>
        bool Remove(TKey key);

        /// <summary>
        /// Добавить значение по ключу
        /// </summary>
        /// <remarks>
        /// Добавляет только уникальные значения
        /// </remarks>
        /// <param name="key">ключ</param>
        /// <param name="value">значение</param>
        void Add(TKey key, TValue value);
    }
}