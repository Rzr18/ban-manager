﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Gt.Base.Extensions;

[assembly: AssemblyTitle("Gt.Base")]
[assembly: AssemblyDescription("Base types and infrastructure")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Global Traffic Service Limited")]
[assembly: AssemblyProduct("Gt.Base")]
[assembly: AssemblyCopyright("Copyright ©  Mikhail Nazarov 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]


[assembly: Guid("6f25f027-a157-43de-89b8-840d79f9423f")]

[assembly: AssemblyVersion("2.1.8")]

