﻿using System;

namespace Gt.Base.Maybees
{
    public sealed class Match<T>
    {
        public T Obj { get; }

        public Match(T obj)
        {
            Obj = obj;
        }

        public bool IsExecuted { get; private set; }

        public Match<T> Case<TCase>(Action<TCase> action) where TCase : T
        {
            if (IsExecuted || !MatchHelper.IsTypeOf<TCase>(Obj)) return this;

            try
            {
                action(MatchHelper.Cast<TCase>(Obj));
            }
            finally
            {
                IsExecuted = true;
            }

            return this;
        }

        public Match<T> Case<TCase>(Action<TCase> action, Predicate<TCase> predicate) where TCase : T
        {
            if (IsExecuted || !MatchHelper.IsTypeOf<TCase>(Obj)) return this;
            var obj = MatchHelper.Cast<TCase>(Obj);
            if (!predicate(obj)) return this;

            try
            {
                action((TCase)Obj);
            }
            finally
            {
                IsExecuted = true;
            }

            return this;
        }

        public void Default(Action<T> action)
        {
            if (!IsExecuted)
                action(Obj);
        }

        public void Default(Action<T> action, Predicate<T> predicate)
        {
            if (!IsExecuted && predicate(Obj))
                action(Obj);
        }
    }

    public sealed class Match<T, TR>
    {
        public T Obj { get; }

        public Match(T obj)
        {
            Obj = obj;
        }

        public Match<T, TR> Case<TCase>(Func<TCase, TR> func) where TCase : T
        {
            if (!Result.HasValue && MatchHelper.IsTypeOf<TCase>(Obj))
                Result = func(MatchHelper.Cast<TCase>(Obj));
            return this;
        }



        public Match<T, TR> Case<TCase>(Func<TCase, TR> func, Predicate<TCase> predicate) where TCase : T
        {
            if (Result.HasValue || !MatchHelper.IsTypeOf<TCase>(Obj)) return this;
            var obj = MatchHelper.Cast<TCase>(Obj);
            if (predicate(obj))
                Result = func((TCase)Obj);
            return this;
        }

        public Match<T, TR> Case(Func<T, TR> func, Predicate<T> predicate)
        {
            if (!Result.HasValue && predicate(Obj))
                Result = func(Obj);
            return this;
        }

        public TR Default(Func<T, TR> func)
        {
            return Result.HasValue 
                ? Result.Value 
                : (Result = func(Obj)).Value;
        }


        public Maybe<TR> Result { get; private set; }

    }
}
