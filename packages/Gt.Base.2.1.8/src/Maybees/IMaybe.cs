namespace Gt.Base.Maybees
{
    /// <summary>
    /// ��������� ������������� ��������
    /// </summary>
    public interface IMaybe
    {
        /// <summary>
        /// ������� ������� ��������
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// �������� ����������� ���������� ���� �������� � ���� <c>T</c>
        /// </summary>
        /// <typeparam name="T">��� ����������</typeparam>
        /// <returns>������� ����������� ���������� ����</returns>
        bool ValueIs<T>();

        /// <summary>
        /// ���������� ���� � <c>T</c>
        /// </summary>
        /// <typeparam name="T">��� ����������</typeparam>
        /// <returns>��������, ����������� � ����</returns>
        T Cast<T>();

        /// <summary>
        /// �������� ��������
        /// </summary>
        /// <returns>��������</returns>
        object GetValue();
    }
}