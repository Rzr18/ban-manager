﻿using System;

namespace Gt.Base.Maybees
{
    internal static class MatchHelper
    {
        public static bool IsTypeOf<T>(object obj)
        {
            if (obj is T) return true;
            return obj
                .TryCast<IMaybe>()
                .Select(m => m.HasValue && m.ValueIs<T>())
                .DefaultIfEmpty(false);
        }

        public static T Cast<T>(object obj)
        {
            if (obj is T)
                return (T)obj;
            return obj.TryCast<IMaybe>().Select(m => m.Cast<T>()).DefaultIfEmpty(new Lazy<T>(() => (T)obj));
        }

    }
}