﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gt.Base.Maybees
{
    /// <summary>
    /// Опциональное значение
    /// </summary>
    /// <remarks>
    /// Может использоваться для типов-значений и ссылочных типов
    /// </remarks>
    /// <typeparam name="T">тип значения</typeparam>
    public struct Maybe<T> : IEquatable<Maybe<T>>, IMaybe
    {
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(Maybe<T> other)
        {
            return Equals(other._value, _value) && other.HasValue.Equals(HasValue);
        }

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <returns>
        /// true if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.
        /// </returns>
        /// <param name="obj">Another object to compare to. </param><filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Maybe<T> && Equals((Maybe<T>)obj);
        }

        /// <summary>
        /// Сравнение maybe-значения и другого значения
        /// </summary>
        public static bool operator ==(Maybe<T> maybe, T other)
        {
            return maybe.HasValue && maybe.Value.Equals(other);
        }

        /// <summary>
        /// Сравнение maybe-значения и другого значения
        /// </summary>
        public static bool operator ==(T other, Maybe<T> maybe)
        {
            return maybe == other;
        }


        /// <summary>
        /// Сравнение maybe-значения и другого значения
        /// </summary>
        public static bool operator !=(Maybe<T> maybe, T other)
        {
            return !(maybe == other);
        }

        /// <summary>
        /// Сравнение maybe-значения и другого значения
        /// </summary>
        public static bool operator !=(T other, Maybe<T> maybe)
        {
            return !(maybe == other);
        }


        /// <summary>
        /// Сравнение двух maybe-значений
        /// </summary>
        public static bool operator ==(Maybe<T> left, Maybe<T> right)
        {
            return left.Equals(right);
        }
        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>
        /// A 32-bit signed integer that is the hash code for this instance.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T>.Default.GetHashCode(_value) * 397) ^ HasValue.GetHashCode();
            }
        }

        /// <summary>
        /// Сравнение двух maybe-значений
        /// </summary>
        public static bool operator !=(Maybe<T> left, Maybe<T> right)
        {
            return !left.Equals(right);
        }

        private readonly T _value;

        /// <summary>
        /// Создание экземпляра <c>Maybe</c>
        /// </summary>
        private Maybe(T item, bool hasValue)
        {
            _value = item;
            HasValue = hasValue;
        }

        /// <summary> Gets the underlying value, if it is available </summary>
        /// <value>The value.</value>
        public T Value
        {
            get
            {
                if (!HasValue)
                    throw new InvalidOperationException();
                return _value;
            }
        }


        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return HasValue
                       ? _value.ToString()
                       : "Empty";
        }

        /// <summary>
        /// Оператор приведения значения к <c>Maybe</c>
        /// </summary>
        public static implicit operator Maybe<T>(T value)
        {
            return new Maybe<T>(value);
        }


        /// <summary>
        /// Оператор извлечения значения из <c>Maybe</c>
        /// </summary>
        public static explicit operator T(Maybe<T> maybe)
        {
            if (!maybe.HasValue)
                throw new InvalidOperationException();
            return maybe.Value;
        }

        internal Maybe(T value)
            : this(value, true)
        {
            if (Equals(value, null))
                throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Признак наличия значения
        /// </summary>
        public bool HasValue { get; }

        /// <summary>
        /// Проверка возможности приведения типа значения к типу <c>T</c>
        /// </summary>
        /// <typeparam name="T1">тип приведения</typeparam>
        /// <returns>признак возможности приведения типа</returns>
        public bool ValueIs<T1>()
        {
            return Value is T1;
        }


        // ReSharper disable StaticFieldInGenericType
        public static readonly Maybe<T> Empty = new Maybe<T>(default(T), false);
        // ReSharper restore StaticFieldInGenericType

        /// <summary>
        /// Приведение типа к <c>T</c>
        /// </summary>
        /// <typeparam name="T1">тип приведения</typeparam>
        /// <returns>значение, приведенное к типу</returns>
        public T1 Cast<T1>()
        {
            return (T1)(object)Value;
        }

        /// <summary>
        /// Получить значение
        /// </summary>
        /// <returns>значение</returns>
        /// <exception cref="InvalidOperationException">в случае отсутствия значения</exception>
        public object GetValue()
        {
            return Value;
        }
    }

    /// <summary>
    /// Методы-расширения для <c>Maybe</c>
    /// </summary>
    public static class Maybe
    {
        /// <summary>
        /// Получить пустое значение <c>Maybe&lt;T&gt;.Empty</c>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Maybe<T> Empty<T>()
        {
            return Maybe<T>.Empty;
        }

        public static int Compare<T>(this Maybe<T> value1, Maybe<T> value2) where T : IComparable
        {
            if (value1 == value2) return 0;
            if (!value1.HasValue) return -1;
            if (!value2.HasValue) return 1;

            return value1.Value.CompareTo(value2.Value);
        }

        /// <summary>
        /// Linq-реализация where для maybe
        /// </summary>
        public static Maybe<T> Where<T>(this Maybe<T> maybe, Func<T, bool> predicate)
        {
            return maybe.HasValue
                       ? (
                            predicate(maybe.Value)
                                ? maybe
                                : Empty<T>()
                         )
                       : Empty<T>();
        }

        /// <summary>
        /// Возвращает себя
        /// </summary>
        public static Maybe<T> Select<T>(this Maybe<T> maybe)
        {
            return maybe;
        }

        /// <summary>
        /// Возвращает результат выполнения функции от maybe-значения, если оно не <c>Empty</c>.
        /// В противном случае возвращает <c>Empty</c> 
        /// </summary>
        public static Maybe<T2> Select<T1, T2>(this Maybe<T1> maybe, Func<T1, T2> function)
        {
            return maybe.HasValue
                ? function(maybe.Value)
                : Maybe<T2>.Empty;
        }


        /// <summary>
        /// Linq-реализация select для maybe
        /// </summary>
        public static Maybe<T2> SelectMany<T1, T2>(this Maybe<T1> maybe,
                                                      Func<T1, Maybe<T2>> function)
        {
            return maybe.HasValue
                ? function(maybe.Value)
                : Maybe<T2>.Empty;
        }

        /// <summary>
        /// Linq-реализация select для maybe
        /// </summary>
        public static Maybe<T3> SelectMany<T1, T2, T3>(this Maybe<T1> optional,
                                                          Func<T1, Maybe<T2>> function,
                                                          Func<T1, T2, T3> combiner)
        {
            if (!optional.HasValue)
                return Empty<T3>();
            var v2 = function(optional.Value);
            if (!v2.HasValue)
                return Empty<T3>();
            return combiner(optional.Value, v2.Value);
        }

        /// <summary>
        /// Возвращает <c>Maybe</c> со значением, равным данному или <c>Empty</c> если значение равно null
        /// </summary>
        public static Maybe<T> AsMaybe<T>(this T item)
        {
            return Equals(item, null)
                ? Empty<T>()
                : new Maybe<T>(item);
        }

        /// <summary>
        /// Возвращает <c>Maybe</c> со значением, равным данному или <c>Empty</c> если значение равно null
        /// </summary>
        public static Maybe<T> AsMaybe<T>(this T? item) where T : struct
        {
            return item ?? Maybe<T>.Empty;
        }

        /// <summary>
        /// Пытается привести значение объекта к указанному типу.
        /// В случае неудачи возвращает <c>Empty</c>
        /// </summary>
        public static Maybe<TTo> TryCast<TTo>(this object from)
        {
            if (@from is TTo)
                return (TTo)@from;
            return Empty<TTo>();
        }

        /// <summary>
        /// Если значение равно <c>Empty</c> возвращает дефолтное значение для типа <c>T</c>
        /// </summary>
        public static T DefaultIfEmpty<T>(this Maybe<T> maybe)
        {
            return DefaultIfEmpty(maybe, default(T));
        }

        /// <summary>
        /// Если значение равно <c>Empty</c> возвращает указанное значение
        /// </summary>
        public static T DefaultIfEmpty<T>(this Maybe<T> maybe, T defaultValue)
        {
            return maybe.HasValue
                ? maybe.Value
                : defaultValue;
        }

        public static T DefaultIfEmpty<T>(this Maybe<T> maybe, Lazy<T> defaultValue)
        {
            return maybe.HasValue
                ? maybe.Value
                : defaultValue.Value;
        }

        public static IEnumerable<T> DefaultIfEmpty<T>(this Maybe<IEnumerable<T>> maybe)
        {
            return maybe.HasValue
                ? maybe.Value
                : Enumerable.Empty<T>();
        }

        /// <summary>
        /// Если значение равно <c>Empty</c> возвращает <c>string.Empty</c>
        /// </summary>
        public static string DefaultIfEmpty(this Maybe<string> str)
        {
            return str.HasValue
                ? str.Value
                : string.Empty;
        }

        /// <summary>
        /// Выполняет указанное действие, если значение не равно <c>Empty</c> 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="maybe"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static Maybe<T> IfNotEmpty<T>(this Maybe<T> maybe, Action<T> action)
        {
            if (maybe.HasValue)
                action(maybe.Value);
            return maybe;
        }

        /// <summary>
        /// Выполняет указанное действие, если значение равно <c>Empty</c>
        /// </summary>
        public static Maybe<T> IfEmpty<T>(this Maybe<T> maybe, Action action)
        {
            if (!maybe.HasValue)
                action();
            return maybe;
        }

        /// <summary>
        /// Возвращает первое значение, если оно не Empty. Иначе: второе
        /// </summary>
        public static Maybe<T> Combine<T>(this Maybe<T> maybe, Maybe<T> other)
        {
            return maybe.HasValue ? maybe : other;
        }

        /// <summary>
        /// Попытка приведения <c>IMaybe</c> к типу <c>T</c>
        /// </summary>
        public static Maybe<T> TryCast<T>(this IMaybe maybe)
        {
            if (!maybe.HasValue || !maybe.ValueIs<T>())
                return Empty<T>();
            return maybe.Cast<T>();
        }

        /// <summary>
        /// Попытка привести к типу <c>T?</c>. В случае неудачи результат: null
        /// </summary>
        public static T? NullIfEmpty<T>(this IMaybe maybe)
           where T : struct
        {
            return maybe.TryCast<T?>().DefaultIfEmpty((T?)null);
        }

        /// <summary>
        /// Возвращает <c>Empty</c>, если указанное значение равно null. Иначе само значение
        /// </summary>
        public static Maybe<T> EmptyIfNull<T>(this T? value)
            where T : struct
        {
            return value ?? Empty<T>();
        }

        /// <summary>
        /// Возвращает <c>Empty</c>, если указанное значение равно null. Иначе само значение
        /// </summary>
        public static Maybe<T> EmptyIfNull<T>(this T value)
            where T : class
        {
            return value ?? Empty<T>();
        }
    }
}
