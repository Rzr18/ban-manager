﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Gt.Base
{
    public class PagedList<T>
    {
        public IEnumerable<T> Items { get; set; }
        public int Total { get; set; }
    }
}