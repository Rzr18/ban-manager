﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Gt.Base.Extensions;
using Newtonsoft.Json;

namespace Gt.Base.Slack
{
    /// <summary>
    /// Класс уведомления в Slack
    /// </summary>
    public class Slack
    {
        private readonly string _userName;
        private readonly string _uri;
        private const string DefaultNotifyRequestUri = @"https://hooks.slack.com/services/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";

        public Slack(string userName, string uri = DefaultNotifyRequestUri)
        {
            _userName = userName;
            _uri = uri ?? DefaultNotifyRequestUri;
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="icon">Иконка уведомления http://www.emoji-cheat-sheet.com/ </param>
        /// <param name="text">Текст уведомления</param>
        /// <param name="fields"></param>
        public Task<bool> Notify(string icon, string text, params SlackField[] fields)
        {
            return SendNotification(_userName, icon, text, _uri, fields);
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="text">Текст уведомления</param>
        /// <param name="fields"></param>
        public Task<bool> Error(string text, params SlackField[] fields)
        {
            return SendNotification(_userName, ":x:", text, _uri, fields);
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        public Task<bool> Error(Exception ex, object details = null)
        {
            if (ex == null) throw new ArgumentNullException(nameof(ex));
            var sb = new StringBuilder();
            var e = ex;

            var fields = new List<SlackField>();

            while (e != null)
            {
                sb.AppendFormat($"{e.Message}\r\n{e.StackTrace}\r\n");
                e = e.InnerException;
            }

            fields.Add(new SlackField
            {
                Short = false,
                Title = "Stack Trace",
                Value = sb.ToString()
            });

            if (ex.Data.Keys.Count>0)
            {
                var data = ex.Data;
                sb = new StringBuilder();
                foreach (var key in data.Keys)
                {
                    var value = data[key];
                    sb.AppendFormat("{0} : {1}\r\n", key, value);
                }
                fields.Add(new SlackField
                {
                    Short = false,
                    Title = "Data",
                    Value = sb.ToString()
                });
            }
            if (details != null)
            {
                fields.Add(new SlackField
                {
                    Short = false,
                    Title = "Details",
                    Value = JsonConvert.SerializeObject(details)
                });
            }

            return Error(ex.Message, fields.Count> 0? fields.ToArray(): null);
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="text">Текст уведомления</param>
        /// <param name="fields"></param>
        public Task<bool> Success(string text, params SlackField[] fields)
        {
            return SendNotification(_userName, ":white_check_mark:", text, _uri, fields);
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="text">Текст уведомления</param>
        /// <param name="fields"></param>
        public Task<bool> Information(string text, params SlackField[] fields)
        {
            return SendNotification(_userName, ":information_source:", text, _uri, fields);
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="icon">Иконка уведомления http://www.emoji-cheat-sheet.com/ </param>
        /// <param name="text">Текст уведомления</param>
        /// <param name="fields"></param>
        public static Task<bool> SendNotification(string username, string icon, string text, params SlackField[] fields)
        {
            return SendNotification(username, icon, text, DefaultNotifyRequestUri, fields);
            
        }

        /// <summary>
        /// Метод отправки уведомления в Slack
        /// </summary>
        /// <param name="username">Имя пользователя</param>
        /// <param name="icon">Иконка уведомления http://www.emoji-cheat-sheet.com/ </param>
        /// <param name="text">Текст уведомления</param>
        /// <param name="uri"></param>
        /// <param name="fields"></param>
        private static async Task<bool> SendNotification(string username, string icon, string text, string uri, params SlackField[] fields)
        {
            try
            {
                using (var client = new HttpClient())
                {

                    var data =
                        new
                        {
                            text,
                            username,
                            icon_emoji = icon,
                            fields
                        };
                    var content = await data.ToContent();
                    var response = await client.PostAsync(uri, content);
                    return response.IsSuccessStatusCode;

                }
            }
            catch
            {
                return false;
            }

        }
    }
}