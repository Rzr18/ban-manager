﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Gt.IoC")]
[assembly: AssemblyDescription("Invertion of control infrastructure")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Global Traffic Service Limited")]
[assembly: AssemblyProduct("Gt.IoC")]
[assembly: AssemblyCopyright("Copyright © Mikhail Nazarov 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("478eb386-cd18-4106-9660-3c2c0f292993")]

[assembly: AssemblyVersion("1.2.0.0")]

