﻿using System;

namespace Gt.IoC
{
    internal class SingletonInfo
    {
        public Type AsType { get; set; }
        public Type Type { get; set; }
        public string Name { get; set; }
    }
}