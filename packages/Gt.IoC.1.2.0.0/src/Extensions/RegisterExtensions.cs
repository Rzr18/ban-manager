namespace Gt.IoC.Extensions
{
    internal static class RegisterExtensions
    {
        public static bool IsScoped(this RegisterAttribute attr) => attr is ScopedAttribute;
        public static bool IsTransient(this RegisterAttribute attr) => attr is TransientAttribute;
        public static bool IsSingleton(this RegisterAttribute attr) => attr is SingletonAttribute;
    }
}