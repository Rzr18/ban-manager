using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Gt.IoC.Extensions
{
    /// <summary>
    /// ������-���������� ��������� ��������
    /// </summary>
    public static class ServicesExtensions
    {
        /// <summary>
        /// ����������� � ������� ��������� � �������������� �� ������, ���������� ��� <c>T</c>
        /// </summary>
        /// <typeparam name="T">��� ������ �������������</typeparam>
        /// <param name="services">��������� ��������</param>
        public static void AddRegisteredWithAttributes<T>(this IServiceCollection services)
        {
            AddRegisteredWithAttributes(services, typeof(T));
        }

        /// <summary>
        /// ����������� � ������� ��������� � �������������� �� ������, ���������� ��������� ���
        /// </summary>
        /// <param name="services">��������� ��������</param>
        /// <param name="assemblyTag">��� ������ �������������</param>
        public static void AddRegisteredWithAttributes(this IServiceCollection services, Type assemblyTag)
        {
            var assembly = assemblyTag.Assembly;
            services.AddRegisteredWithAttributes(assembly);
            services.AddDependencies(assemblyTag);
        }

        /// <summary>
        /// ����������� � ������� ��������� ����� � ��������� ������
        /// </summary>
        /// <param name="services">��������� ��������</param>
        /// <param name="assembly">������, � ������� �������������� ����� ����� ��� �����������</param>
        public static void AddRegisteredWithAttributes(this IServiceCollection services, Assembly assembly)
        {
            services.AddRegisteredWithAttributesCore(assembly);
        }

        private static void AddDependencies(this IServiceCollection services, IReflect assemblyTag)
        {
            var property = assemblyTag.GetProperty("Dependencies", BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.Public);
            var depTypes = property?.GetValue(null) as IEnumerable<Type>;
            if (depTypes == null) return;

            foreach (var depType in depTypes)
            {
                services.AddRegisteredWithAttributes(depType);
            }
        }

        private static void AddRegisteredWithAttributesCore(this IServiceCollection services, Assembly assembly)
        {
            
            var types = assembly.DefinedTypes.SelectMany(t =>
                t.GetCustomAttributes(typeof(RegisterAttribute), false).Cast<RegisterAttribute>().Select(x => new
                {
                    Type = t,
                    Attr = x
                }))
            .Where(x => x.Attr != null);
           
            foreach (var t in types)
            {
                var type = t.Type;
                var interfaces = type.GetDirectInterfaces().ToList();
                
                interfaces.Remove(typeof(IDisposable));
                if (!interfaces.Any() || t.Attr.AsType != null)
                {
                    var asType = t.Attr.AsType ?? type;
                    if (t.Attr.IsTransient())
                        services.AddTransient(asType, type, t.Attr.Name);
                    else if (t.Attr.IsScoped())
                        services.AddScoped(asType, type, t.Attr.Name);
                    else if (t.Attr.IsSingleton())
                        services.AddSingleton(asType, type, t.Attr.Name);
                }
                else
                {
                    foreach (var @interface in interfaces)
                    {
                        if (t.Attr.IsTransient())
                            services.AddTransient(@interface, t.Type, t.Attr.Name);
                        else if (t.Attr.IsScoped())
                            services.AddScoped(@interface, type, t.Attr.Name);
                        else if (t.Attr.IsSingleton())
                            services.AddSingleton(@interface, type, t.Attr.Name);
                    }
                }

            }

            
        }

        /// <summary>
        /// ��������� ������� ���������� ����
        /// </summary>
        /// <typeparam name="T">��� �������</typeparam>
        /// <param name="services">��������� ��������</param>
        /// <returns></returns>
        public static T GetService<T>(this IServiceProvider services)
        {
            return (T)services.GetService(typeof (T));
        }
    }
}