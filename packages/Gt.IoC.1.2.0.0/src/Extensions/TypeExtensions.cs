﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gt.IoC.Extensions
{
    internal static class TypeExtensions
    {
        public static IEnumerable<Type> GetDirectInterfaces(this Type type)
        {
            var interfaces = type.GetInterfaces();
            var baseInterfaces = interfaces.SelectMany(i => i.GetInterfaces());
            if (type.BaseType != null)
            {
                baseInterfaces = baseInterfaces.Concat(type.BaseType.GetInterfaces());
            }
            return interfaces.Except(baseInterfaces);
        }

    }
}