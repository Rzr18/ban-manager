﻿using System;

namespace Gt.IoC
{
    public interface IServiceCollection
    {
        void AddScoped(Type type, string name=null);
        void AddScoped(Type iface, Type type, string name=null);
        void AddSingleton(Type type, object instance, string name=null);
        void AddSingleton(Type type, Type impl, string name = null);
        void AddTransient(Type iface, Type type, string name = null);
    }
    
}
