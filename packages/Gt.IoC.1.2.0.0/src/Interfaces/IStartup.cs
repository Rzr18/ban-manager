namespace Gt.IoC
{
    public interface IStartup
    {
        void Startup();
        void ResolveSingletones();
    }
}