﻿using System;
using System.Collections.Generic;

namespace Gt.IoC
{
    /// <summary>
    /// Расширенный интерфейс провайдера сервисов
    /// </summary>
    public interface IServiceProvider2 : IServiceProvider
    {
        /// <summary>
        /// Получить сервис по типу <c>T</c>
        /// </summary>
        /// <typeparam name="T">тип сервиса</typeparam>
        /// <param name="name">имя регистрации (опционально)</param>
        /// <returns>сервис</returns>
        T GetService<T>(string name=null);

        /// <summary>
        /// Получить все сервисы <c>T</c>
        /// </summary>
        /// <typeparam name="T">тип сервисов</typeparam>
        /// <returns>коллекция сервисов</returns>
        IEnumerable<T> GetAllServices<T>();

    }
}