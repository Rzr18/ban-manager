﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.Unity;

namespace Gt.IoC
{
    public abstract class IocContainer: IServiceCollection, IServiceProvider2, IStartup
    {
        private readonly object _sync = new object();
        private readonly List<StartupDescriptor>  _startups=new List<StartupDescriptor>();

        protected IUnityContainer UnityContainer { get; }

        protected IocContainer(IUnityContainer container)
        {
            UnityContainer = container;
            AddSingleton(typeof(IServiceProvider), this);
            AddSingleton(typeof(IServiceCollection), this);
            AddSingleton(typeof(IServiceProvider2), this);
            AddSingleton(typeof (IStartup), this);
        }

        public void AddScoped(Type type, string name = null)
        {
            UnityContainer.RegisterType(type, name, new HierarchicalLifetimeManager());
        }

        public void AddScoped(Type iface, Type type, string name = null)
        {
            UnityContainer.RegisterType(iface, type,name, new HierarchicalLifetimeManager());
        }

        private void AddStartups(Type asType, Type type, string name)
        {
            var startupMethods = type.GetMethods().Select(x => new
            {
                Attr = x.GetCustomAttributes<StartupAttribute>().FirstOrDefault(),
                Method = x
            })
                    .Where(x => x.Attr != null)
                    .Select(x => x.Method)
                    .ToList();
            lock (_sync)
            {
                _startups.AddRange(startupMethods.Select(m => new StartupDescriptor
                {
                    Type = asType,
                    Method = m,
                    Name = name
                }));
            }
        }

        public void AddSingleton(Type asType, Type type, string name = null)
        {
            UnityContainer.RegisterType(asType, type, name, new ContainerControlledLifetimeManager());
            AddStartups(asType, type, name);
        }

        public void AddSingleton(Type asType, object instance, string name=null)
        {
            var type = instance.GetType();
            UnityContainer.RegisterInstance(asType, name, instance);
            AddStartups(asType, type, name);
        }


        public void AddTransient(Type iface, Type type, string name=null)
        {
            UnityContainer.RegisterType(iface, type,name, new TransientLifetimeManager());
        }

        public T GetService<T>(string name = null)
        {
            return UnityContainer.Resolve<T>(name);
        }

        public IEnumerable<T> GetAllServices<T>()
        {
            return UnityContainer.ResolveAll<T>();
        }

        public abstract object GetService(Type serviceType);

        public void Startup()
        {
            IEnumerable<StartupDescriptor> startups;
            lock (_sync)
            {
                startups = _startups.ToList();
                _startups.Clear();
            }
            
            foreach (var startupInfo in startups)
            {
                var obj = UnityContainer.Resolve(startupInfo.Type, startupInfo.Name);
                startupInfo.Method.Invoke(obj, null);
            }
        }

        public void ResolveSingletones()
        {
            UnityContainer.Registrations.Where(
                r => r.LifetimeManagerType == typeof (ContainerControlledLifetimeManager))
                .Select(x => UnityContainer.Resolve(x.RegisteredType,
                    // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                    x.Name)).ToList();
        }
    }
}
