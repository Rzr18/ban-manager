using System;

namespace Gt.IoC
{
    /// <summary>
    /// ����������� �� ������� ��������� (� WebApi �� ������ ����� �����������) 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]  
    public class ScopedAttribute : RegisterAttribute {}
}