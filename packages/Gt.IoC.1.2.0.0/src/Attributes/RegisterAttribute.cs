using System;

namespace Gt.IoC
{
    /// <summary>
    /// ������� ����� �������� �����������
    /// </summary>
    public abstract class RegisterAttribute : Attribute
    {
        /// <summary>
        /// ���, � �������� �������� ����� ��������������� ���, �� ������� ������� �������
        /// </summary>
        public Type AsType { get; set; }

        /// <summary>
        /// ��� ����������� ��� ������������� ����������� (�����������)
        /// </summary>
        public string Name { get; set; }
    }
}