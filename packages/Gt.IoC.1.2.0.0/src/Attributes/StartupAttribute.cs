using System;

namespace Gt.IoC
{
    /// <summary>
    /// ������� ����������� ������ ���������
    /// </summary>
    [AttributeUsage(AttributeTargets.Method,AllowMultiple = true,Inherited = false)]
    public class StartupAttribute: Attribute { }
}