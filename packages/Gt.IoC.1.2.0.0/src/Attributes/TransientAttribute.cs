using System;

namespace Gt.IoC
{
    /// <summary>
    /// ����������� ��� �������� ������ ������� �� ������ ����� Resolve()
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class TransientAttribute : RegisterAttribute{}
}