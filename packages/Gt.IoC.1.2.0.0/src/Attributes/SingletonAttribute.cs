using System;

namespace Gt.IoC
{
    /// <summary>
    /// ����������� ���������
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class SingletonAttribute : RegisterAttribute {}
}