﻿using System;
using System.Reflection;

namespace Gt.IoC
{
    internal class StartupDescriptor
    {
        public Type Type { get; set; }
        public string Name { get; set; }
        public MethodInfo Method { get; set; }
    }
}