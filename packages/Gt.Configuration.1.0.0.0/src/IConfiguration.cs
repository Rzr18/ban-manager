﻿using System.Threading.Tasks;

namespace Gt.Configuration
{
    public interface IConfiguration
    {
        T Get<T>(string key);
        string Get(string key);

        Task Set(string key, string value);
        Task Set<T>(string key, T obj);
    }
}
