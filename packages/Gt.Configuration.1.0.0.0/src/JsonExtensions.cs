﻿using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Gt.Configuration
{
    internal static class JsonExtensions
    {

        public static Task<string> ToJson<T>(this T obj, Formatting formatting=Formatting.None)
        {
            return Task.Factory.StartNew(
                                            () => formatting==Formatting.None 
                                                ? JsonConvert.SerializeObject(obj) 
                                                : JsonConvert.SerializeObject(obj, formatting)
                                        );
        }
    }
}
