using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Gt.Configuration
{
    public class Configuration : IConfiguration
    {
        private readonly JObject _config;
        private readonly string _fileName;

        public Configuration(string fileName)
        {
            _fileName = fileName;
            var text = File.ReadAllText(fileName);
            _config = JsonConvert.DeserializeObject<JObject>(text);
        }

        public Task Set<T>(string key, T obj)
        {
            var props = typeof (T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var prop in props)
            {
                var propName = prop.GetCustomAttributes<JsonPropertyAttribute>().FirstOrDefault()?.PropertyName ??
                               prop.Name;
                var propValue = prop.GetValue(obj);

                var strValue = JsonConvert.SerializeObject(propValue);
                SetInternal(propName, strValue);
            }
            return Save();
        }

        public T Get<T>(string key)
        {
            var value = Get(key);
            if (value == null) return default(T);
            if (typeof(T).IsPrimitive)
            {

                return (T)Convert.ChangeType(value, typeof(T));

            }
            return JsonConvert.DeserializeObject<T>(value);
        }

        private void SetInternal(string key, string value)
        {
            var pathItems = key.Split(':');
            if (pathItems.Length == 0) throw new ArgumentOutOfRangeException(nameof(key));

            var section = _config;
            JToken token;

            for (var index = 0; index < pathItems.Length - 1; index++)
            {
                var pathItem = pathItems[index];

                if (!section.TryGetValue(pathItem, out token))
                {
                    token = new JObject();
                    section.Add(pathItem, token);
                }
                section = token as JObject;
                if (section == null) 
                    throw new ArgumentOutOfRangeException(nameof(key), "invalid key specified");
            }
            var lastItem = pathItems.Last();

            if (!section.TryGetValue(lastItem, out token))
            {
                token = new JValue(lastItem);
                section.Add(lastItem, token);
            }
            var v = token as JValue;
            if (v != null)
            {
                v.Value = value;
                return;
            }
            throw new ArgumentOutOfRangeException(nameof(key), "Only set of JValue config values supported");
        }

        public Task Set(string key, string value)
        {
            SetInternal(key, value);
            return Save();
        }

        public string Get(string key)
        {
            var pathItems = key.Split(':');
            if (pathItems.Length == 0) return null;

            var section = _config;
            JToken token;

            for (var index = 0; index < pathItems.Length - 1; index++)
            {
                var pathItem = pathItems[index];

                if (!section.TryGetValue(pathItem, out token)) return null;
                section = token as JObject;
                if (section == null) return null;
            }

            if (!section.TryGetValue(pathItems.Last(), out token)) return null;
            var v = token as JValue;
            if (v != null) return v.Value.ToString();

            var obj = token as JObject;
            return obj?.ToString(Formatting.None);
        }

        private async Task Save()
        {

            var value = await _config.ToJson(Formatting.Indented);

            using (TextWriter writer = new StreamWriter(File.OpenWrite(_fileName)))
            {
                await writer.WriteAsync(value);
            }
        }
    }
}