﻿using System;
using System.Threading.Tasks;

namespace BanManager.Logics
{
    public interface IMessage
    {
        Task Info(string message,string details=null);
        Task Error(string message, string details=null);
        Task Tgrm(string message, string details = null);
        Task Error(Exception ex);
    }
}