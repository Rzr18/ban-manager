﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanManager.Logics
{
    public interface IBanListLogic
    {
        /// <summary>
        /// добавить в бан лист
        /// </summary>
        /// <param name="address">IP адрес</param>
        void Add(string address);

        /// <summary>
        /// Проверить актуальность бан листа</summary>
        void CheckActual();

        /// <summary>
        /// Очистить бан лист
        /// </summary>
        void Clear();

        object Summary { get; }
    }
}
