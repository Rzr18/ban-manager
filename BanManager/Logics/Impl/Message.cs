using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Gt.Base;
using Gt.Base.Slack;
using Gt.Configuration;
using Gt.IoC;
using Newtonsoft.Json;
using Telegram.Bot;

namespace BanManager.Logics.Impl
{
    public class TelegramParams
    {
        public string Token { get; set; }
        public UInt32[] Chats { get; set; }
    }

    public class MessageParams
    {
        public string[] Emails { get; set; }
        public string[] Phones { get; set; }
        public TelegramParams Telegram { get; set; }
    }

    [Scoped]
    public class Message : IMessage
    {
        private readonly Slack _slack=new Slack("Traff Mon");
        private readonly EmailSender _emailSender;
        private readonly string[] _emails;
        private readonly string[] _phonses;
        private readonly string _serviceName;

        private readonly TelegramParams _telegram;
        private TelegramBotClient _telegramBot; 

        public Message(IConfiguration config)
        {
            _serviceName = config.Get("ServiceName");
            if (string.IsNullOrEmpty(_serviceName) == false)
                _serviceName += ": ";

            var cfg= config.Get<MessageParams>("Messages");
            _emails = cfg.Emails;
            _phonses = cfg.Phones;
            _telegram = cfg.Telegram;
            _telegramBot = new TelegramBotClient(_telegram.Token);

            _emailSender = new EmailSender(new EmailSenderOptions
            {
                FromEmail = "parowozof@yandex.ru",
                EnableSsl = true,
                Port = 25,
                Host = "smtp.yandex.ru",
                UseDefaultCredentials = false,
                UserName = "parowozof",
                Password = "Mavr211354"
            });
        }

        public async Task Info(string message, string details)
        {
            SendSlack(message);

            SendEmail(message, details);

            SendSms(message);

            SendTelegram(message);
        }

        private async Task SendSms(string message)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    var phones = string.Join(",", _phonses);
                    var res = await
                            httpClient.GetStringAsync(
                                $"http://sms.ru/sms/send?api_id=28D855F7-23A9-A976-2671-2CB1F4A2EB07&to={phones}&text={message}");
                    var parts = res.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length < 1 || parts[0] != "100")
                    {
                        await _slack.Error("������ �������� SMS");
                    }
                }
            }
            catch (Exception) { }
        }

        private async Task SendTelegram(string message)
        {
            try { 
                foreach(var chat in _telegram.Chats)
                    await _telegramBot.SendTextMessageAsync(chat, _serviceName + message);
            }
            catch (Exception) { }
        }

        private async Task SendSlack(string message)
        {
            try
            {
                await _slack.Error(message);
            }
            catch (Exception) { }
        }

        private async Task SendEmail(string message, string details)
        {
            try
            {
                foreach (var email in _emails)
                {
                    await _emailSender.SendEmailAsync(email, _serviceName + message, details ?? string.Empty);
                }
            }
            catch (Exception) { }
        }

        public async Task Error(string message, string details)
        {
            SendSlack(message);

            SendEmail(message, details);

            SendSms(message);

            SendTelegram(message);
        }

        public async Task ErrorTgrm(string message, string details)
        {
            await SendTelegram(message);
        }

        private readonly Dictionary<string, DateTime> _errors=new Dictionary<string, DateTime>();

        public async Task Error(Exception ex)
        {
            DateTime time;
            if (_errors.TryGetValue(ex.Message, out time) && (DateTime.Now - time).TotalDays < 1)
                return;
            _errors.Add(ex.Message, DateTime.Now);

            await _slack.Error(ex);
            foreach (var email in _emails)
            {
                await _emailSender.SendEmailAsync(email,"������ ��� ������ ������� �����������", ex.Format());
            }
        }

        public async Task Tgrm(string message, string details = null)
        {
            await SendTelegram(message + "\n" + details);
        }
    }
}