﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using TraffMon.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace TraffMon.Logics.Impl
{
    public class AgentsCountChecker
    {
        public string Uri { get; set; }
        public string Name { get; set; }
        public ChState State { get; set; } = new ChState();
        public string ErrorMessage { get; set; }

        private DateTime _CheckTime;
        public DateTime CheckTime => _CheckTime;

        private AgentsCountModel _prevagents;

        public event EventHandler StateChanged;

        public AgentsCountChecker()
        {
            State.ChangesValue += OnStateChanged;
        }

        public void OnStateChanged(object sender, EventArgs args)
        {
            StateChanged(this, null);
        }

        public async Task<AgentsCountModel> GetAgents()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(15);

                    var response = await client.GetStringAsync(Uri);
                    var obj = JsonConvert.DeserializeObject<AgentsCountModel>(response);

                    return obj;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "Ошибка получения агентов: " + ex.Message;
                return null;
            }
        }

        public async Task Check()
        {
            AgentsCountModel agents = await GetAgents();
            if (agents == null)
            {
                _CheckTime = DateTime.Now;
                State.setRefusal();
            }

            if (_prevagents != null && agents.Data.SequenceEqual(_prevagents.Data))
                return;
            _prevagents = agents;

            bool? bFail = CheckRefusal(agents, out string det);

            if (bFail == null)
            {
                ErrorMessage = "Недостаточно данных для анализа";
                return;
            }
            if (bFail == true)
            {
                ErrorMessage = "Сбой: " + det;
                State.setRefusal();
            }
            else
            {
                ErrorMessage = "";
                State.setNormal();
            }
            _CheckTime = DateTime.Now;
        }

        /// <summary>
        /// Расчет стандартного отклонения
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        static public double StandartDeviation(int[] array)
        {
            double avrg = array.Average();
            var summ = array.Select(x => (x - avrg) * (x - avrg)).Sum();
            return Math.Sqrt(((double)1 / (array.Count() - 1)) * summ);
        }

        static public Dictionary<string, int?> GetLastValues(AgentsCountModel mod)
        {
            // убираем нули в конце
            var nonnull = mod.Data.Reverse().SkipWhile(x => x.Value == null).ToArray();
            // отрезаем лишнее, оставляя 5 элементов
            if(nonnull.Count() >= 5)
            {
                var last5 = nonnull.Reverse().Skip(nonnull.Count() - 5).ToArray();

                return last5.ToDictionary(x => x.Key, y => y.Value);
            }

            return null;
        }

        static public bool? CheckRefusal(AgentsCountModel mod, out string Details)
        {
            Details = string.Empty;
            
            var lastValues = GetLastValues(mod);
            if (lastValues == null)
                return null;
            foreach(var lv in lastValues)
                Details += $"{lv.Key} - {lv.Value}\n";

            if (lastValues.Count(x => x.Value == null) > 0)
                return null;

            var arr = lastValues.Select(x => (int)x.Value).ToArray();
            var StDev = StandartDeviation(arr);

            Details += $"\nСт.Откл.={Convert.ToInt32(StDev)} Max={arr.Max()}";
            var list = lastValues.ToList();

            return (StDev > (arr.Max() / 50)) && 
                list[list.Count-1].Value < list[list.Count-2].Value;
        }
    }
}