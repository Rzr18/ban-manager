﻿using System;
using System.Collections.Generic;
using System.IO;
using Gt.WebApi;
using Newtonsoft.Json;
using TraffMon.Models;

namespace TraffMon.Logics.Impl
{
    internal static class AgentsFormatter
    {
        private static readonly string Tpl;

        static AgentsFormatter()
        {
            var path = new HostingEnvironment().MapServerPath("App_Data/body.html");


            Tpl = File.ReadAllText(path);
        }

        private static readonly TimeZoneInfo Tz = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

        public static string Format(AgentsCountModel model)
        {
            var data = new List<object>();
            
            
            
            var start = DateTime.Today;

            for (int i = 0; i < 144; i++)
            {
                var time = start.AddMinutes(10*i);
                data.Add(new
                {
                    Time = time.ToString("HH:mm"),
                    Value = model.Data[time.ToString("HH:mm")]
                });
            }
            
            return Tpl.Replace("{title}","Агенты").Replace("{data}", JsonConvert.SerializeObject(data));
        }

        public static string FormatDays(AgentsCountModel model)
        {
            var data = new List<object>();

            foreach (var d in model.Data)
            {
                data.Add(new
                {
                    Time = d.Key, d.Value
                });
            }
            

            return Tpl.Replace("{title}", "Агенты по дням").Replace("{data}", JsonConvert.SerializeObject(data));
        }
    }
}