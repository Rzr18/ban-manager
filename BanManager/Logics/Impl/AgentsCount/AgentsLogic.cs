﻿using System;
using System.Collections;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Gt.IoC;
using Newtonsoft.Json;
using TraffMon.Models;
using System.Diagnostics;

using Telegram.Bot;

using System.Diagnostics;

namespace TraffMon.Logics.Impl
{
    [Singleton]
    public class AgentsLogic : IAgentsLogic
    {
        private readonly IMessage _message;

        public AgentsLogic(IMessage message)
        {
            if (message == null) throw new ArgumentNullException(nameof(message));
            _message = message;
            foreach (var ch in Checkers)
                ch.StateChanged += OnStateChanged;
        }

        private static readonly TelegramBotClient Bot = new TelegramBotClient("473372338:AAELG3iaqLrM8V_4MLl5_AdozHF_HKPcdIA");
        private AgentsCountChecker[] Checkers =
        {
            new AgentsCountChecker(){ Uri = "http://2.vkhost.tk:81/agents/online/hour2?day=0", Name = "Мастер2.Агенты" },
            new AgentsCountChecker(){ Uri = "http://khost.tk:81/agents/online/hour2?day=0", Name = "Мастер1.Агенты" }
        };

        public IEnumerable Summary => Checkers.Select(v => new { v.Uri, v.Name, State = v.State.Current, v.CheckTime, v.ErrorMessage });

        public async Task Check()
        {
            try
            {
                foreach(var ch in Checkers)
                {
                    await ch.Check();
                }
            }catch(Exception ex)
            {
                await _message.Tgrm("AgentsLogic.Check(): " + ex.Message);
            }
        }

        public void OnStateChanged(object sender, EventArgs e)
        {
            AgentsCountChecker ch = sender as AgentsCountChecker;
            Debug.Assert(ch != null);
            if (ch == null)
                return;
            if(ch.State.Current == ChStates.Refusal)
                _message.Tgrm($"Сбой - количество агентов {ch.Name}: {ch.ErrorMessage}");
            else
                _message.Tgrm($"Норма - количество агентов {ch.Name}: {ch.ErrorMessage}");
        }

        private TimeZoneInfo _tz = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");


        private int FindIndex()
        {
            var now = TimeZoneInfo.ConvertTime(DateTime.UtcNow, _tz);
            var index = ((int)(now - now.Date).TotalMinutes)/10+18;
            return index;
        }

        public async Task<AgentsCountModel> GetAgents()
        {
            var url = "http://vkhost.tk:81/agents/online/hour2?day=0";
            var url2 = "http://2.vkhost.tk:81/agents/online/hour2?day=0";
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromSeconds(15);

                    var response = await client.GetStringAsync(url);
                    var obj = JsonConvert.DeserializeObject<AgentsCountModel>(response);

                    response = await client.GetStringAsync(url2);
                    var obj2 = JsonConvert.DeserializeObject<AgentsCountModel>(response);

                    foreach (var i in obj2.Data)
                    {
                        if (!obj.Data.TryGetValue(i.Key, out int? value))
                            obj.Data[i.Key] = i.Value;
                        else
                            obj.Data[i.Key] +=  i.Value;
                    }
                    return obj;
                }
            }
            catch (Exception ex)
            {
                //await _message.Error(ex);
                throw;
            }
        }

        public async Task<AgentsCountModel> GetAgentsByDays()
        {

            var url = "http://vkhost.tk:81/agents/days?days=60";
            try
            {
                using (var client = new HttpClient())
                {

                    var response = await client.GetStringAsync(url);
                    var obj = JsonConvert.DeserializeObject<AgentsCountModel>(response);
                    return obj;
                }
            }
            catch (Exception ex)
            {
                await _message.Error(ex);
                return null;
            }
        }
    }
}