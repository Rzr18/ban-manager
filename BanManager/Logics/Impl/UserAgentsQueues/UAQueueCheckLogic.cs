﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Diagnostics;
using Gt.Base.Extensions;
using Gt.IoC;
using TraffMon.Models;

namespace TraffMon.Logics.Impl
{
    public sealed class RequestInf
    {
        public string LocationId { get; set; }
        public string BrowserName { get; set; }

        public RequestInf()
        {
            LocationId = "63ccb4b1-f257-44bf-bcac-c7e0d8681921";
            BrowserName = "Chrome";
        }
    }

    public class RSize
    {
        private int Width { get; set; }
        private int Height { get; set; }
    }

    public class ProxyInf
    {
        public string Address { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Type { get; set; }
    }

    public sealed class AnswerInf
    {
        public int CookieId { get; set; }
        public RSize Resolution { get; set; }
        public string UserAgent { get; set; }
        public ProxyInf Proxy;
    }

    public class HttpPostRequestor : ICheckRequestor
    {
        public object PostRequest(string uri, string body)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    RequestInf req = new RequestInf();
                    Task<AnswerInf> answ = client.PostAsync<AnswerInf>(uri, req);
                    
                    return answ.Result;
                }
            }
            catch(Exception)
            {
                return null;
            }
        }
    }

    [Singleton]
    public class UAQueueCheckLogic : IUAQueueCheckLogic
    {
        private readonly IMessage _message;
        private static ICheckRequestor _requestor = new HttpPostRequestor();
        private const int _checkPeriod = 20;

        private UriChecker[] _Checkers =
        {
            new UriChecker("http://mobile.queue.jet-serv.com/visit", Properties.Resources.UACheckReqBody, _checkPeriod, _requestor),
            new UriChecker("http://default.queue.jet-serv.com/visit", Properties.Resources.UACheckReqBody, _checkPeriod, _requestor),
            new UriChecker("http://default.queue.office61.ru/visit", Properties.Resources.UACheckReqBody, _checkPeriod, _requestor),
            new UriChecker("http://mobile.queue.office61.ru/visit", Properties.Resources.UACheckReqBody, _checkPeriod, _requestor)
        };

        public UAQueueCheckLogic(IMessage message)
        {
            _message = message ?? throw new ArgumentNullException(nameof(message));

            foreach (var uch in _Checkers)
                uch.StateChangesEvent += OnStateChanges;
        }

        private void OnStateChanges(string uri, string msg, ChState state)
        {
            _message.Error($"{msg} - {uri}");
        }

        public UAQueueInfo[] Summary { get {
                return _Checkers.Select(x => new UAQueueInfo { Uri = x.Uri, CheckTime = x.CheckTime, State = x.State.Current }).ToArray();
            }
        }

        public void Check()
        {
            foreach (var uch in _Checkers)
               uch.Check();
        }
    }

    public enum ChStates
    {
        Unknown,
        Normal,
        Refusal,
        Unstable
    }

    public class ChState
    {
        public event EventHandler ChangesValue;
        protected ChStates _CurState = ChStates.Unknown;
        public virtual ChStates Current
        {
            get => _CurState;
            set
            {
                if (_CurState == value)
                    return;
                if (_CurState == ChStates.Unknown && value == ChStates.Normal)
                {
                    _CurState = value;
                    //return;
                }

                _CurState = value;
                OnChangesEvent();
            }
        }

        public void setNormal()
            {
            Current = ChStates.Normal;
        }

        public void setRefusal()
        {
            Current = ChStates.Refusal;
        }

        public void OnChangesEvent()
        {
            ChangesValue?.Invoke(this, new EventArgs());
        }
    }

    public class ChStateCross: ChState
    {
        public override ChStates Current {
            get => _CurState;
            set
            {
                if (_CurState == value)
                    return;
                if (_CurState == ChStates.Unknown && value == ChStates.Normal)
                {
                    _CurState = value;
                    return;
                }

                if (CrossingState == value)
                {
                    if (++CrossingPoints >= CrossingPointsRequirements)
                    {
                        _CurState = value;
                        OnChangesEvent();
                    }
                }
                else
                {
                    CrossingState = value;
                    _CurState = ChStates.Unstable;
                    CrossingPoints = 1;
                }
            } }

        public ChStateCross(int crossPointReqrmnt = 1)
        {
            CrossingPointsRequirements = crossPointReqrmnt;
        }

        private int CrossingPointsRequirements;
        private int CrossingPoints;
        private ChStates CrossingState;
    }

    public interface ICheckRequestor
    {
        object PostRequest(string uri, string body);
    }

    public delegate void StateChangesEvent(string uri, string msg, ChState state);

    public class UriChecker
    {
        private string _uri;
        public string Uri => _uri;
        private readonly string _body;
        private readonly int _normalCheckPeriod;
        private int _normalPeriodCounter;
        private readonly ICheckRequestor _requestor;
        private bool _WasRefusal = false;

        private DateTime _CheckTime;
        public DateTime CheckTime => _CheckTime;

        protected ChState _State = new ChStateCross(3);
        public ChState State => _State;

        public event StateChangesEvent StateChangesEvent;

        public UriChecker(string uri, string body, int normalCheckPeriod, ICheckRequestor requestor)
        {
            _uri = uri;
            _body = body;
            _normalCheckPeriod = normalCheckPeriod;
            _requestor = requestor;

            State.ChangesValue += OnStateChanges;
        }

        public void Check(bool forse = false)
        {
            if (forse || State.Current != ChStates.Normal)
            {
                CheckState();
                return;
            }

            // пропускаем часть вызовов
            if (++_normalPeriodCounter < _normalCheckPeriod)
                return;

            CheckState();
            _normalPeriodCounter = 0;
        }

        private void CheckState()
        {
            _CheckTime = DateTime.Now;
            //Trace.WriteLine($"Check {_uri} at {_CheckTime.ToLongTimeString()}");
            object res = _requestor.PostRequest(_uri, _body);
            if (res == null)
                State.setRefusal();
            else
                State.setNormal();
        }

        private void OnStateChanges(object sender, EventArgs e)
        {
            if (sender is ChState == false)
                return;
            string message = string.Empty;
            ChState st = sender as ChState;

            // переход в отказ
            if (st.Current == ChStates.Refusal)
            {
                _WasRefusal = true;     // запомнили, что был сбой
                StateChangesEvent?.Invoke(_uri, "Сбой", st); // оповещение
            }
            // переход в норму, при условии, что был отказ 
            // (переход в норму из нестабильности - без оповещения)
            else if (st.Current == ChStates.Normal && _WasRefusal)
            {
                _WasRefusal = false;
                StateChangesEvent?.Invoke(_uri, "Норма", st);
            }
        }
    }
}
