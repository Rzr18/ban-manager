﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading;
using Gt.Configuration;
using Gt.IoC;
using Newtonsoft.Json;
using Microsoft.Win32;

namespace BanManager.Logics.Impl
{
    [Singleton]
    public class BanListLogic : IBanListLogic
    {
        private Timer _timer;
        private DateTime _LastCheckTime;
        private readonly IMessage _message;

        public BanListLogic(IMessage msgr)
        {
            _message = msgr;
            _timer = new Timer(OnTimer, null, TimeSpan.FromSeconds(0), TimeSpan.FromHours(1));
        }

        private void OnTimer(object state)
        {
            _LastCheckTime = DateTime.Now;
            CheckActual();
        }

        public object Summary => new { BanList = RegBanList.GetItems(), LastCheck = _LastCheckTime };

        public void Add(string address)
        {
            if (RegBanList.Exist(address))
                return;
            Firewall.AddToBanList(address);
            RegBanList.Add(address);

            _message.Tgrm("banned " + address);
        }

        public void CheckActual()
        {
            try
            {
                Dictionary<string, string> items = RegBanList.GetItems();
                foreach (var kv in items)
                {
                    DateTime ban_time = DateTime.Parse(kv.Value);
                    if (kv.Value != null && DateTime.Now - ban_time > TimeSpan.FromDays(1))
                    {
                        RegBanList.Remove(kv.Key);
                        Firewall.RemoveFromBanList(kv.Key);
                    }
                }
            }
            catch(Exception ex)
            { }
        }

        public void Clear()
        {
            Firewall.DeleteRule();
            RegBanList.Clear();
        }
    }
}