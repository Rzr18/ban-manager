﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetFwTypeLib;

namespace BanManager
{
    public static class Firewall
    {
        public const string _RuleName = "AutoBanListIP";

        public static void CheckForEnable()
        {
            INetFwMgr firewallMgr = (INetFwMgr)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwMgr", false));

            if (firewallMgr.LocalPolicy.CurrentProfile.FirewallEnabled == false)
            {
                firewallMgr.LocalPolicy.CurrentProfile.FirewallEnabled = true;
            }
        }

        public static void CreateRule(string address)
        {
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
            INetFwRule rule = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FWRule"));
            rule.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK; // Действие правила
            rule.Enabled = true; // Включить
            rule.InterfaceTypes = "All"; // Профили
            rule.RemoteAddresses = address;
            rule.Name = _RuleName; // Название

            firewallPolicy.Rules.Add(rule);
        }

        public static void DeleteRule()
        {
            INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));

            firewallPolicy.Rules.Remove(_RuleName);
        }

        public static INetFwRule GetRule()
        {
            try
            {
                INetFwPolicy2 firewallPolicy = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
                INetFwRule rule = firewallPolicy.Rules.Item(_RuleName);
                if (rule != null)
                    return rule;
            }
            catch (Exception)
            {
            }

            return null;
        }

        public static void AddToBanList(string address)
        {
            try
            {
                INetFwRule rule = GetRule();
                if (rule == null)
                {
                    CreateRule(address);
                    return;
                }

                string[] addresses = rule.RemoteAddresses.Split(',');
                if (addresses.Contains(address))
                    return;
                if (rule.RemoteAddresses == string.Empty || rule.RemoteAddresses == "*")
                    rule.RemoteAddresses = address;
                else
                    rule.RemoteAddresses += "," + address;
            }
            catch (Exception ex)
            {
                throw new Exception("FW: " + ex.Message);
            }
        }

        public static void RemoveFromBanList(string address)
        {
            try
            {
                INetFwRule rule = GetRule();

                if (rule.RemoteAddresses.Contains(address))
                {
                    string[] addresses = rule.RemoteAddresses.Split(',');
                    string remadrs = string.Join(",", addresses.Where(a => a.Contains(address) == false).ToArray());
                    if (remadrs == string.Empty)
                        DeleteRule();
                    else
                        rule.RemoteAddresses = remadrs;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("FW: " + ex.Message);
            }
        }
    }
}