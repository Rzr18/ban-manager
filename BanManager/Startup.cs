﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Http;
using Gt.WebApi.Extensions;
using Microsoft.Owin;
using Owin;
using BanManager;
using BanManager.Logics;
using System.Diagnostics;

[assembly: OwinStartup(typeof(Startup))]

namespace BanManager
{
    public class Startup
    {
        private Timer _timer;

        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.ConfigureApi(typeof(Startup));
            
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);

            config.Startup();
            _timer = new Timer(OnTimer, config,TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(1));
        }

        private void OnTimer(object state)
        {
            var config = (HttpConfiguration) state;
           
        }

        public static IEnumerable<Type> Dependencies => new Type[0];
    }
}