﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Win32;

namespace BanManager
{
    public static class RegBanList
    {
        private static RegistryKey GetKey(bool write = false)
        {
            try
            {
                RegistryKey currentUserKey = Registry.CurrentUser;
                RegistryKey key = currentUserKey.OpenSubKey(Firewall._RuleName, write);
                if (key == null)
                    key = currentUserKey.CreateSubKey(Firewall._RuleName);
                return key;
            }
            catch(Exception ex)
            {
                throw new Exception("RegBanList.GetKey: " + ex.Message);
            }
        }

        public static bool Exist(string address){
            RegistryKey key = GetKey();
            string[] names = key.GetValueNames();
            return names.Contains(address);
        }

        public static void Add(string address)
        {
            if (Exist(address))
                return;
            RegistryKey key = GetKey(true);
            key.SetValue(address, DateTime.Now);
        }

        public static Dictionary<string, string> GetItems()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            RegistryKey key = GetKey();
            string[] names = key.GetValueNames();
            foreach(var n in names)
            {
                res.Add(n, key.GetValue(n).ToString());
            }
            return res;
        }

        public static void Remove(string address)
        {
            if (Exist(address))
            {
                RegistryKey key = GetKey(true);
                key.DeleteValue(address);
            }
        }

        public static void Clear()
        {
            RegistryKey key = GetKey(true);
            string[] names = key.GetValueNames();
            foreach (var n in names)
            {
                key.DeleteValue(n);
            }
        }
    }
}