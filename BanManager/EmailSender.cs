﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BanManager
{
    internal class EmailSender
    {
        private readonly EmailSenderOptions _options;


        public EmailSender(EmailSenderOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));
            _options = options;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            
            var mm = new MailMessage
            {
                Subject = subject,
                Body = subject,
                IsBodyHtml = true,
                From = new MailAddress(_options.FromEmail)
            };
            mm.Attachments.Add(Attachment.CreateAttachmentFromString(message,"visits.html"));
            mm.To.Add(email);

            var smtp = new SmtpClient
            {
                Host = _options.Host,
                Port = _options.Port,
                EnableSsl = _options.EnableSsl,
                UseDefaultCredentials = _options.UseDefaultCredentials,
                Credentials = new System.Net.NetworkCredential(_options.UserName, _options.Password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            
            return smtp.SendMailAsync(mm);

        }
    }
}