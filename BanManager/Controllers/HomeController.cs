﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using BanManager.Logics;
using System.Reflection;
using BanManager.Logics.Impl;

namespace BanManager.Controllers
{
    [RoutePrefix("")]
    public class HomeController : ApiController
    {
        private readonly IBanListLogic _logic;

        public HomeController(IBanListLogic logic)
        {
            _logic = logic;
        }

        [Route("summary"), HttpGet]
        public IHttpActionResult summary()
        {
            return Json(_logic.Summary);
        }

        [Route("version"), HttpGet]
        public IHttpActionResult Version()
        {
            return Json(Assembly.GetAssembly(this.GetType()).GetName().Version);
        }

        [Route("ban"), HttpPut]
        public IHttpActionResult Ban(string address)
        {
            try
            {
                _logic.Add(address);
                return Json(RegBanList.GetItems());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        [Route("clear"), HttpPut]
        public IHttpActionResult Clear()
        {
            try
            {
                _logic.Clear();
                return Json(RegBanList.GetItems());
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}